#### Microsoft API references ###
In order to use this program, you must create an app id and app secret through the Azure App Directory

(Source for the following: https://docs.microsoft.com/en-us/graph/tutorials/python?tutorial-step=2 )

1. Go to https://aad.portal.azure.com/
2. Use MSOE single-sign on to sign in (I was automatically signed in on this site.)
3. Go to Azure Active Directory
4. Select New Registration
5. **Be sure to set "Supported Account Types" to "Accounts in any organizational directory and personal Microsoft accounts."**
   The authentication will fail otherwise
6. Set up a Web redirect UI to http://localhost:8000/tutorial/gettoken/ **This is different than the tutorial.  I have configure this app to use this value.**
7. Register.
8. Copy the Application ID 
9. Select Certificates & secrets.  Create a new client secret  Make a secret that expires in one year. (Probably good for security purposes...)
10. **Be sure to copy the client secret (also known as the app secret) before closing the page**

Paste these into an oauth_settings.yml file.

### Python packages needed ###
Run these commands:

    pip install pandas==1.1.2
    pip install requests==2.24.0
    pip install Django==3.1.1
    pip install numpy==1.19.2

You could alternatively run

    pip install -r requirements.txt

The requirements.txt file in this repository was produced with
```
pip install pipreqs
cd <this project>
pipreqs
```

### Other packages you don't need any more ###
```
# For Fall 2020 OAuth work:
pip install requests_oauthlib==1.3.0
pip install pyyaml==5.3.1
pip install python-dateutil==2.8.1
```

#### Running the program ####
To test emails, run

    tutorial/file_parser.py

as a script.

To send emails, run

    manage.py runserver
    
then browse to

    localhost:8000

```127.0.0.1:8000``` does NOT work because that IP address cannot be registered with Microsoft as a valid redirect URL.


# To test sending a single email
In ```tutorial/views.py/send_mail```, change which function call is commented out to ```send_test_message()```.

Set file_parse.py SETUP_FILE to None for safety.

# To test sending all emails
* Standard: Run file_parser.py by itself.
* To actually see emails: Either: 
  * Manually set student's emails to your own in the tab-delimited .txt file
  * Edit ```file_parser.py``` to read ```[INSTRUCTORS_EMAIL]``` instead of ```[INSTRUCTORS_EMAIL, email]``` when
  calling ```outlookservice.send_message```.


# Known bugs:
* For some reason, sending an email WITH AN ATTACHMENT to MULTIPLE PEOPLE causes a bizarre error.
  Workaround: Remove the "cc to self" when sending attachments: Replace ```[INSTRUCTORS_EMAIL, email]``` with
   ```[email]``` in the call to ```outlookservice.send_message``` in ```file_parser.py```.
   
   
# References
https://stackoverflow.com/a/58000704/1048186
https://docs.microsoft.com/en-us/graph/tutorials/python
https://docs.microsoft.com/en-us/graph/tutorials/python?tutorial-step=1
https://docs.microsoft.com/en-us/graph/tutorials/python?tutorial-step=2

# Confusion over version
The tutorial uses the old v1.0 Azure Active Directory Authentication Library (ADAL) 
instead of the new v2.0 Microsoft Authentication Library (MSAL).

I'm not sure which version I'm using. I follow the ADAL tutorial to create my keys, but my queries
included an embedded v2.0 in the URL.

I'm fine with continuing to use v1.0 (ADAL) even though they are not
developing it, because it is still supported and used in the tutorial. More info:
https://docs.microsoft.com/en-us/azure/active-directory/develop/migrate-python-adal-msal

