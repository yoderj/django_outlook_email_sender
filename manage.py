#!/usr/bin/env python
"""Django's command-line utility for administrative tasks.

RUN THIS SCRIPT with runserver as a parameter, then browse to
localhost:8000
(127.0.0.1:8000 does NOT work.)
"""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_outlook_email_sender.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    print('Just for convenience: http://localhost:8000 (THe later link when it starts does not include the localhost '
          'domain.  Use this link instead, once it\'s started.')
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
