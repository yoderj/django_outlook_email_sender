import sys

import pandas as pd
import numpy as np
import os

from tutorial import outlookservice

SETUP_FILE = r'C:\Dropbox\msoe-ferpa\class\se3030\21q3\gradeReports' \
             r'\SE3030Final.tab'
# SETUP_FILE = r'C:\Dropbox\msoe-ferpa\class\cs3450\21q3\gradeReports\.tab'
# SETUP_FILE = r'C:\Dropbox\msoe-ferpa\class\cs2911\21q1\gradeReports\CS291121q2Week12Saturday.tab'

# NEW USERS: Start by seeing if this will work for you:
# SETUP_FILE = r'../exampleSetupFiles/CS2911Quiz2Week11All041Setup.tab'

DEBUG = False
PREVIEW_PLAINTEXT = True # When True, don't show the row start/end in the output. (But WILL be
# included when sending email!)
SEND_TO_INSTRUCTOR_ONLY = False
INSTRUCTOR_EMAIL = 'yoder@msoe.edu'

def parse_setup():
    table = pd.read_csv(SETUP_FILE, sep='\t', dtype=str, index_col='Name')
    table = table.transpose()
    global DIRECTORY
    global GRADE_TABLES
    global ROW_START # Indent used for standard rows to make headers stand out
    global ROW_END # Separation after each field is printed
    global LINE_END # Within a field, separator used for \n's within the user's data. Defaults to ROW_END
    global INTRODUCTION
    global SUBJECT
    global ATTACHMENT_PREFIX
    global ATTACHMENT_POSTFIX
    global ATTACHMENT_PREFIX_IS_RELATIVE
    global INCLUDE_NUMBERS
    ATTACHMENT_PREFIX = None
    ATTACHMENT_POSTFIX = None
    ATTACHMENT_PREFIX_IS_RELATIVE = False
    INCLUDE_NUMBERS = True
    DIRECTORY = os.path.dirname(SETUP_FILE)
    # One grade table in each column. The rest is hard-coded.
    GRADE_TABLES = [DIRECTORY + '/' + table_name for table_name in table['Table'].values.tolist()]
    ROW_START = table['RowStart'].values.tolist()[0]
    ROW_END = table['RowEnd'].values.tolist()[0]
    LINE_END = ROW_END
    INTRODUCTION = table['Introduction'].values.tolist()[0]
    SUBJECT = table['Subject'].values.tolist()[0]
    if 'AttachmentPrefix' in table.columns:
        ATTACHMENT_PREFIX = table['AttachmentPrefix'].values.tolist()[0]
        # The middle is the username in lower-case, extracted from the email.
        # For details on creating PDFs from quizzes, see ~/bin/splitpdf
        # For an example using it, see 20q1/cs2911/gradeReports/CS2911Quiz2Week11All041Setup.tab
        #
        # For an example of splitting foggp_passepartaut-style team names, see
        # dupgraded in /c/Dropbox/msoe-ferpa/class/cs2911/21q1/Labs/Labs5and6/
        ATTACHMENT_POSTFIX = table['AttachmentPostfix'].values.tolist()[0]
        ATTACHMENT_PREFIX_IS_RELATIVE = (table['AttachmentPrefixIsRelative'].values.tolist()[0].lower() == "true")
        print("ATTACHMENT_PREFIX_IS_RELATIVE:",ATTACHMENT_PREFIX_IS_RELATIVE)
    if 'LineEnd' in table.columns:
        LINE_END = table['LineEnd'].values.tolist()[0]
    if 'IncludeNumbers' in table.columns:
        INCLUDE_NUMBERS = table['IncludeNumbers'].values.tolist()[0].lower() == 'true'

class SenderException(Exception):
    """
    Indicates problem on single student line.
    (Exception vs Error: Following Java distinction)
    """
    pass


class SenderError(Exception):
    """
    Indicates bigger problem - must stop now
    (I don't catch/'except' this error)
    (Exception vs Error: Following Java distinction)
    """
    pass


def send_emails(access_token = None):
    include_markup = True
    if not access_token and PREVIEW_PLAINTEXT:
        include_markup = False

    if include_markup:
        brlf = '<br>\n'
    else:
        brlf = '\n'

    parse_setup()
    total_emails = 0
    errors = []
    for table_name in GRADE_TABLES:
        table = pd.read_csv(table_name, sep='\t', dtype=str, encoding = "ISO-8859-1")
        headers = table.iloc[2, :]
        grades = table.iloc[3:, :]
        column_name = None
        column_points = None

        for column_name, column_points in headers[:-2].items():
            if type(column_name) is not str:
                raise SenderError('EARLY CATCH: All column names should be strings. Found column with name:', column_name,
                                  'among columns', headers)
            if not (type(column_points) is str and (
                        column_points.lower().startswith('header') or
                        column_points.lower().startswith('comment'))):
                try:
                    # This confirms the column will look good printed as, e.g., 15/20 (value/column_points)
                    float(column_points)
                    if type(column_points) is float and np.isnan(column_points):
                        raise SenderError('EARLY CATCH: nan column type:', column_points, 'for header',column_name,
                                          'among columns', headers)
                except ValueError:
                    try:
                        if column_points.strip().endswith('%'):
                            float(column_points.strip()[:-1])
                        else:
                            raise SenderError('EARLY CATCH: Unrecognized header type:', column_points, 'for column',
                                              column_name, 'among columns', headers)
                    except ValueError:
                        raise SenderError('EARLY CATCH: Unrecognized header type:', column_points, 'for column',
                                      column_name, 'among columns', headers)

        # Last two columns must be exactly as checked here
        assert(np.all(headers.iloc[-2:].index == ['First Name', 'Email']))

        total_emails += grades.shape[0]
        for _, row in grades.iterrows():
            try:
                if DEBUG:
                    print('row',row)
                message = ''
                name = row[-2]
                email = row[-1]
                if SEND_TO_INSTRUCTOR_ONLY:
                    email = INSTRUCTOR_EMAIL
                if type(email) is not str:
                    raise SenderException('Skipping row with invalid email: name:',name,'row:',row)
                if type(name) is not str:
                    raise SenderException('Skipping row with invalid student name:',row)
                print('='*50)
                print('='*50)
                print('name:',name,'email:',email)
                print('subject:',SUBJECT)
                attachment = get_attachment(email)
                if attachment:
                    print('attachment:',attachment)
                print('='*50)
                print()

                if attachment:
                    try:
                        open(attachment,'r')
                    except FileNotFoundError:
                        print('Could not find the attachment: ', attachment)
                        raise SenderException('Could not find the attachment: ',attachment,' for row ',row)

                message += name+','+brlf+brlf

                introduction = INTRODUCTION
                introduction = ((introduction.replace('\n', LINE_END + '\n') + ROW_END + '\n')
                                if include_markup else introduction + '\n')

                message += introduction + brlf+brlf
                # message += '<br>\n<br>\n'

                prev_column_name = ''
                for ((column_name, column_points), (_, value)) in zip(headers.iloc[:-2].items(), row[:-2].items()):
                    # Undo column de-duplication: Remove the .1 appended to duplicate column names by Pandas
                    # This is for where two columns in a row include first a numeric grade, then comments
                    # on the same subject.
                    if column_name == prev_column_name + '.1':
                        column_name = prev_column_name
                    if DEBUG:
                        print('index:', column_name, 'header:', column_points, 'value:', value, sep='\t')
                    if type(column_points) is str and column_points.lower().startswith('header'):
                        if include_markup:
                            message += '<h1>' + column_name + '</h1>\n'
                        else:
                            message += '\n'+ column_name + '\n'
                    elif type(column_points) is str and column_points.lower().startswith('comment'):
                        if type(value) is float and np.isnan(value):
                            pass  # We skip comments with empty values below
                        elif type(value) is not str:
                                raise SenderException('Skipping row with non-string value',value,
                                                      'for column',column_name,'in row',row,
                                                      'This can happen when a student has a blank in a numeric column.')
                        elif type(column_name) is not str:
                            raise SenderError('LATE CATCH: All column names should be strings. Found column with name:',column_name,
                                              'among columns',headers,
                                              '(An EARLY CATCH exception should have been thrown above)')
                        else:
                            message += ((ROW_START+'<b>' if include_markup else '  ')+
                                        column_name+('</b>' if include_markup else '')+': '
                                        + ((value.replace('\n',
                                                          LINE_END+'\n'+ROW_START)+ROW_END+'\n')
                                                                        if include_markup else
                                           value.replace('\n','\n  ')+'\n')
                                        )
                    else:
                        if type(value) is not str:
                            raise SenderException('Skipping email with non-string value', value,
                                                  'for column', column_name, 'in row', row,
                                                  'Because file is opened with dtype string, this should not occur.')
                        if type(column_points) is not str:
                            raise SenderException('Skipping email with non-string column_points', column_points,
                                                  'for column', column_name, 'in row', row,
                                                  'Because file is opened with dtype string, this should not occur.')
                        if INCLUDE_NUMBERS:
                            message += ((ROW_START+'<b>' if include_markup else '  ')
                                        +column_name
                                        +('</b>' if include_markup else '')+': '+value+'/'+column_points
                                        +(ROW_END if include_markup else '')+"\n"
                                       )
                    prev_column_name = column_name

                message += brlf+brlf+'Please contact me with any questions.'+brlf+brlf

                message += 'Thank you!'+brlf+brlf+'\nDr. Yoder'

                print(message)
                if access_token:
                    # , email
                    success, response = outlookservice.send_message(access_token, [email],
                                                               SUBJECT, message, attachment)
                    print('response from server:', response)
                    if not success:
                        raise SenderException('Server error while attempting to send message:',response,'for student',name,email)
            except SenderException as e:
                print('Skipping email.',e)
                errors.append(e)

    if len(errors) == 0:
        print('All',total_emails,'emails processed without detecting formatting errors')
    else:
        print('Skipped',len(errors),'of',total_emails,'emails:',file=sys.stderr)
        for e in errors:
            print('Skipped:',e,file=sys.stderr)
            print(file=sys.stderr)
            print(file=sys.stderr)
        print('Skipped',len(errors),'of',total_emails,'emails:',file=sys.stderr)


def get_attachment(email):
    """
    :param email: email address of the student for whom to find the attachment. (Username is assumed to come before @)
    :return: None or the attachment if configure to compute one in the globals.
    """
    attachment = None
    if ATTACHMENT_PREFIX is not None:
        attachment = ATTACHMENT_PREFIX + email[0:email.index('@')] + ATTACHMENT_POSTFIX
        if ATTACHMENT_PREFIX_IS_RELATIVE:
            attachment = DIRECTORY + '/' + attachment
    return attachment


if __name__ == '__main__':
    send_emails()