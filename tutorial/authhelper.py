from urllib.parse import quote, urlencode
import requests
import base64
import json
import time
import os
import yaml
from requests_oauthlib import OAuth2Session

# Unused new stuff
# This is necessary for testing with non-HTTPS localhost
# Remove this if deploying to production
# os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
# # This is necessary because Azure does not guarantee
# to return scopes in the same case and order as requested
# os.environ['OAUTHLIB_RELAX_TOKEN_SCOPE'] = '1'
# os.environ['OAUTHLIB_IGNORE_SCOPE_CHANGE'] = '1'

# Load the oauth_settings.yml file
# (New)
stream = open('oauth_settings.yml', 'r')
settings = yaml.load(stream, yaml.SafeLoader)
# authorize_url = '{0}{1}'.format(settings['authority'], settings['authorize_endpoint'])
# token_url = '{0}{1}'.format(settings['authority'], settings['token_endpoint'])

# # Client ID and secret
client_id = settings['app_id']
client_secret = settings['app_secret']

# Constant strings for OAuth2 flow
# The OAuth authority
authority = 'https://login.microsoftonline.com'

# Old ones
# # The authorize URL that initiates the OAuth2 client credential flow for admin consent
authorize_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/authorize?{0}')
#
# # The token issuing endpoint
token_url = '{0}{1}'.format(authority, '/common/oauth2/v2.0/token')

# The scopes required by the app
scopes = ['openid',
          'offline_access',
          'User.Read',
          'Mail.Read']


def get_app_id():
    return client_id


# Method to generate a sign-in url
# New methods:
# def get_signin_url():
#   # Initialize the OAuth client
#   aad_auth = OAuth2Session(settings['app_id'],
#     scope=settings['scopes'],
#     redirect_uri=settings['redirect'])
#
#   sign_in_url, state = aad_auth.authorization_url(authorize_url, prompt='login')
#
#   return sign_in_url, state

# Method to exchange auth code for access token
# def get_token_from_code(callback_url, expected_state):
#   # Initialize the OAuth client
#   aad_auth = OAuth2Session(settings['app_id'],
#     state=expected_state,
#     scope=settings['scopes'],
#     redirect_uri=settings['redirect'])
#
#   token = aad_auth.fetch_token(token_url,
#     client_secret = settings['app_secret'],
#     authorization_response=callback_url)
#
#   return token

# Old ones
def get_signin_url(redirect_uri):
    # Build the query parameters for the signin url
    params = {'client_id': client_id,
              'redirect_uri': redirect_uri,
              'response_type': 'code',
              'scope': ' '.join(str(i) for i in scopes)
              }

    signin_url = authorize_url.format(urlencode(params))

    return signin_url


def get_token_from_code(auth_code, redirect_uri):
    # Build the post form for the token request
    post_data = {'grant_type': 'authorization_code',
                 'code': auth_code,
                 'redirect_uri': redirect_uri,
                 'scope': ' '.join(str(i) for i in scopes),
                 'client_id': client_id,
                 'client_secret': client_secret
                 }

    r = requests.post(token_url, data=post_data)

    try:
        return r.json()
    except:
        return 'Error retrieving token: {0} - {1}'.format(r.status_code, r.text)


def get_token_from_refresh_token(refresh_token, redirect_uri):
    # raise NotImplemented('TODO!')
    # Build the post form for the token request
    post_data = {'grant_type': 'refresh_token',
                 'refresh_token': refresh_token,
                 'redirect_uri': redirect_uri,
                 'scope': ' '.join(str(i) for i in scopes),
                 'client_id': client_id,
                 'client_secret': client_secret
                 }

    r = requests.post(token_url, data=post_data)

    try:
        return r.json()
    except:
        return 'Error retrieving token: {0} - {1}'.format(r.status_code, r.text)


def get_access_token(request, redirect_uri):
    current_token = request.session['access_token']
    expiration = request.session['token_expires']
    now = int(time.time())
    if (current_token and now < expiration):
        # Token still valid
        return current_token
    else:
        # Token expired
        # raise NotImplemented('TODO!')
        refresh_token = request.session['refresh_token']
        new_tokens = get_token_from_refresh_token(refresh_token, redirect_uri)

        # Update session
        # expires_in is in seconds
        # Get current timestamp (seconds since Unix Epoch) and
        # add expires_in to get expiration time
        # Subtract 5 minutes to allow for clock differences
        expiration = int(time.time()) + new_tokens['expires_in'] - 300

        # Save the token in the session
        request.session['access_token'] = new_tokens['access_token']
        request.session['refresh_token'] = new_tokens['refresh_token']
        request.session['token_expires'] = expiration

        return new_tokens['access_token']
