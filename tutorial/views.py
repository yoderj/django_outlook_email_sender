from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
import time
import tutorial.file_parser as file_parser
import tutorial.authhelper

# Create your views here.
from django.urls import reverse

from tutorial.authhelper import get_signin_url, get_token_from_code, get_access_token
from tutorial.outlookservice import get_me, get_my_messages, send_test_message, send_message


def home(request):
    # sign_in_url = '#'
    # context = { 'signin_url': sign_in_url }
    # return render(request, 'tutorial/home.html', context)
    redirect_uri = request.build_absolute_uri(reverse('tutorial:gettoken'))
    sign_in_url = get_signin_url(redirect_uri)
    app_id = tutorial.authhelper.get_app_id()
    context = {'signin_url': sign_in_url, 'redirect_uri': redirect_uri, 'app_id': app_id}
    return render(request, 'tutorial/home.html', context)


def gettoken(request):
    auth_code = request.GET['code']
    redirect_uri = request.build_absolute_uri(reverse('tutorial:gettoken'))
    token = get_token_from_code(auth_code, redirect_uri)
    try:
        access_token = token['access_token']
    except KeyError as e:
        print('Auth code:',auth_code)
        print('Token:',token)
        raise RuntimeError('Could not log into the server.',e)
    user = get_me(access_token)
    refresh_token = token['refresh_token']
    expires_in = token['expires_in']

    # expires_in is in seconds
    # Get current timestamp (seconds since Unix Epoch) and
    # add expires_in to get expiration time
    # Subtract 5 minutes to allow for clock differences
    expiration = int(time.time()) + expires_in - 300

    # Save the token in the session
    request.session['access_token'] = access_token
    request.session['refresh_token'] = refresh_token
    request.session['token_expires'] = expiration
    return HttpResponseRedirect(reverse('tutorial:send_mail'))


def view_mail(request):
    access_token = get_access_token(request, request.build_absolute_uri(reverse('tutorial:gettoken')))
    # If there is no token in the session, redirect to home
    if not access_token:
        return HttpResponseRedirect(reverse('tutorial:home'))
    else:
        messages = get_my_messages(access_token)
        context = {'messages': messages['value']}
        return render(request, 'tutorial/mail.html', context)


def send_mail(request):
    access_token = get_access_token(request, request.build_absolute_uri(reverse('tutorial:gettoken')))
    # If there is no token in the session, redirect to home
    if not access_token:
        return HttpResponseRedirect(reverse('tutorial:home'))
    else:
        # send_test_message(access_token)
        file_parser.send_emails(access_token)
        return HttpResponse('Please see console')  # render(request, 'tutorial/mail.html', context) #messages # "{0}: {1}".format(r.status_code, r.text)
