import base64
import mimetypes

import requests
import uuid
import json

import ntpath # For consistent /\ handling on all OS's. https://stackoverflow.com/a/8384788/1048186

graph_endpoint = 'https://graph.microsoft.com/v1.0{0}'

# Generic API Sending
def make_api_call(method, url, token, payload = None, parameters = None):
  # Send these headers with all API calls
  headers = { 'User-Agent' : 'python_tutorial/1.0',
              'Authorization' : 'Bearer {0}'.format(token),
              'Accept' : 'application/json' }

  # Use these headers to instrument calls. Makes it easier
  # to correlate requests and responses in case of problems
  # and is a recommended best practice.
  request_id = str(uuid.uuid4())
  instrumentation = { 'client-request-id' : request_id,
                      'return-client-request-id' : 'true' }

  headers.update(instrumentation)

  response = None

  print('method:',method)
  print('url:',url)
  print('headers:',headers)
  print('params:',parameters)

  if (method.upper() == 'GET'):
      response = requests.get(url, headers = headers, params = parameters)
  elif (method.upper() == 'DELETE'):
      response = requests.delete(url, headers = headers, params = parameters)
  elif (method.upper() == 'PATCH'):
      headers.update({ 'Content-Type' : 'application/json' })
      response = requests.patch(url, headers = headers, data = json.dumps(payload), params = parameters)
  elif (method.upper() == 'POST'):
      headers.update({ 'Content-Type' : 'application/json' })
      response = requests.post(url, headers = headers, data = json.dumps(payload), params = parameters)

  return response

def get_me(access_token):
  get_me_url = graph_endpoint.format('/me')

  # Use OData query parameters to control the results
  #  - Only return the displayName and mail fields
  query_parameters = {'$select': 'displayName,mail'}

  r = make_api_call('GET', get_me_url, access_token, "", parameters = query_parameters)

  if (r.status_code == requests.codes.ok):
    return r.json()
  else:
    return "{0}: {1}".format(r.status_code, r.text)

def get_my_messages(access_token):
  # account = OutlookAccount(access_token)
  # inbox = account.inbox()
  # print("inbox",inbox)
  # https: // outlook.office.com / api / v2.0 / me / MailFolders / ' + folder_name + ' / messages
  #   return HttpResponse('User: {0}, Access token: {1}'.format(user['displayName'], access_token))
  get_messages_url = graph_endpoint.format('/me/mailfolders/inbox/messages')
  print("get_messages_url",get_messages_url)

  # Use OData query parameters to control the results
  #  - Only first 10 results returned
  #  - Only return the ReceivedDateTime, Subject, and From fields
  #  - Sort the results by the ReceivedDateTime field in descending order
  query_parameters = {'$top': '10',
                      '$select': 'receivedDateTime,subject,from',
                      '$orderby': 'receivedDateTime DESC'}

  r = make_api_call('GET', get_messages_url, access_token, parameters=query_parameters)

  if (r.status_code == requests.codes.ok):
    return r.json()
  else:
    return "{0}: {1}".format(r.status_code, r.text)

def send_message(access_token,recipients,subject,body,attachments = None):
    # account = OutlookAccount(access_token)
    # inbox = account.inbox()
    # print("inbox",inbox)
    # https: // outlook.office.com / api / v2.0 / me / MailFolders / ' + folder_name + ' / messages
    #   return HttpResponse('User: {0}, Access token: {1}'.format(user['displayName'], access_token))
    get_messages_url = graph_endpoint.format('/me/sendMail')
    print("get_messages_url", get_messages_url)

    # Create recipient list in required format.
    recipient_list = [{'EmailAddress': {'Address': address}}
                      for address in recipients]

    # Create list of attachments in required format.
    attached_files = []
    if attachments:
        # Handle single attachment if offered instead of list:
        if not isinstance(attachments,list):
            attachments = [attachments]
        for filename in attachments:
            b64_content = base64.b64encode(open(filename, 'rb').read())
            mime_type = mimetypes.guess_type(filename)[0]
            mime_type = mime_type if mime_type else ''
            attached_files.append(
                {'@odata.type': '#microsoft.graph.fileAttachment',
                 'ContentBytes': b64_content.decode('utf-8'),
                 'ContentType': mime_type,
                 'Name': ntpath.basename(filename)})

    # Create email message in required format.
    email_msg = {'Message': {'Subject': subject,
                             'Body': {'ContentType': 'html', 'Content': body},
                             'ToRecipients': recipient_list,
                             'Attachments': attached_files},
                 'SaveToSentItems': 'true'}
    send_message_url = graph_endpoint.format('/me/microsoft.graph.sendMail')
    print('send_message_url:', send_message_url)
    r = make_api_call('POST', send_message_url, access_token, payload=email_msg)
    # Do a POST to Graph's sendMail API and return the response.
    # return session.post(api_endpoint('me/microsoft.graph.sendMail'),
    #                     headers={'Content-Type': 'application/json'},
    #                     json=email_msg)

    if (r.status_code == requests.codes.accepted):
        return (True, "Message was accepted by the server.")
    else:
        return (False ,"{0}: {1}".format(r.status_code, r.text))


def send_test_message(access_token):
    print("Sending test message")
    print('Result:',send_message(access_token,['yoder@msoe.edu'],'TEST MESSAGE','SIMPLE TEST',['kiddos.jpg']))
